import 'package:flutter/material.dart';
import 'package:ubus_map/front_map.dart';

void main() {
  // Moment.setLocaleGlobally(LocaleTh());
      runApp(MyApp());

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light(),
      home: FrontMap(),
    );
  }
}
