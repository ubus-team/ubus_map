import 'dart:async';
import 'dart:convert';
import 'dart:ui';
import 'dart:math' as math;
import 'package:dio/dio.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:latlong/latlong.dart';
import 'package:lottie/lottie.dart' as lottie;
import 'package:marquee/marquee.dart';
import 'package:timer_builder/timer_builder.dart';

import 'package:ubus_map/model/car_position.dart';
import 'package:ubus_map/model/station_position.dart';

class FrontMap extends StatefulWidget {
  @override
  _FrontMapState createState() => _FrontMapState();
}

class _FrontMapState extends State<FrontMap> with TickerProviderStateMixin {

  static const double OFFSET_LAT = -0.00029; // Horizontal Coord
  static const double OFFSET_LON = -0.00005; //Vertical Coord


  List<CarPosition> _customPosition = [];
  List<StationPosition> _stationPosition = [];
  Map<dynamic, dynamic> _stationData = {};
  StreamSubscription<List<CarPosition>> _positionStreamSubscription;
  final Dio dio = Dio();


  AnimationController rotationController;
  final double _defaultLat = 13.96617;
  final double _defaultLon = 100.58470;

  // final double _defaultLat = 13.96578;
  // final double _defaultLon = 100.58542;

  MapController _mapController = MapController();
  double _zoom = 17;

  double ensureDouble(dynamic d) {
    if (d is String) return double.parse(d);
    return double.parse(d.toString());
  }

  String getSystemTime() {
    var now = DateTime.now();
    return DateFormat("HH:mm:ss").format(now);
  }

  clockNow() {
    return TimerBuilder.periodic(Duration(seconds: 1), builder: (context) {
      return Text(
        "${getSystemTime()}",
        style: TextStyle(
            color: Colors.indigo,
            fontSize: 30,
            fontWeight: FontWeight.w700),
      );
    });
  }

  Future<List<StationPosition>> stationPosition() async {
    var response = await dio.get('https://ubus.scotty1944.net/v1/station/list');
    Map<String, dynamic> map = response.data;
    _stationData = map;
    print(map);
    List<StationPosition> stationTemp = map.entries.map<StationPosition>((e) {
      var data = e.value;
      // print('API LAT: $lat');
      // print('API LON: $lon');
      // print('DATA TYPE: Is double ${lon is double}');

      double lat = ensureDouble(data['lat']);
      double lon = ensureDouble(data['lng']);

      print(e.value);



      return StationPosition(
        order: data['order'],
        name: data['name'],
        desc: data['desc'],
        lat: (lat ?? 0) + OFFSET_LAT,
        lon: (lon ?? 0) + OFFSET_LON,
      );
    }).toList();
    return stationTemp;
  }

  fetchStation() async {
    _stationPosition = await stationPosition();
    print(_stationPosition);
    setState(() {});

  }

  Stream<List<CarPosition>> fetchPosition() async* {
    while (true) {
      // final response = await http.get('http://103.212.181.44:3500/location');
      final response = await dio.get('https://ubus.scotty1944.net/location');
      if (response.statusCode == 200) {
        Map<String, dynamic> map = response.data;
        // print('MAP $map');
        yield map.entries.map<CarPosition>((e) {
          var data = e.value;
          // print('API LAT: $lat');
          // print('API LON: $lon');
          // print('DATA TYPE: Is double ${lon is double}');

          double head = ensureDouble(data['head']);
          double lat = ensureDouble(data['lat']);
          double lon = ensureDouble(data['lng']);

          return CarPosition(
            token: e.key,
            name: data['name'],
            desc: data['desc'],
            heading: head,
            lat: lat ?? 0,
            lon: lon ?? 0,
            latestStation: data['station']
          );
        }).toList();
      } else {
        throw Exception('Failed to load post');
      }
      await Future.delayed(Duration(seconds: 1));
    }
  }



  @override
  Widget build(BuildContext context) {


    LatLng _center = new LatLng(_defaultLat, _defaultLon);


    var loc = _customPosition.map<MarkerLayerOptions>((e) {

      var metaInfo = Text(
        e.name ?? 'Unknown',
        style: TextStyle(color: Colors.pinkAccent, fontSize: 10),
      );

      var arrowAnimate = Transform.translate(
        offset: Offset(0, 0),
        child: Container(
          child: lottie.Lottie.asset(
              'assets/lottie/arrow.json',
              // 'assets/lottie/loc_pin.json',
              width: 28,
              height: 28,
              repeat: true,
              fit: BoxFit.cover,
              alignment: Alignment.topCenter
          ),
        ),
      );

      var arrowStack = Stack(
        alignment: Alignment.centerRight,
        children: [
          arrowAnimate,
          // Text(e.name ?? '', style: TextStyle(
          //   color: Colors.indigo,
          //   fontWeight: FontWeight.bold,
          //   shadows: [Shadow(
          //     color: Colors.white,
          //   )]
          // ),),
        ],
      );

      var navigateIcon = Icon(
        Icons.navigation_rounded,
        color: Colors.white,
        size: 14,
      );

      var col = Stack(
        alignment: Alignment.center,
        children: [
          // metaInfo,
          // arrowStack,

          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [BoxShadow(
                color: Colors.black.withAlpha(50),

                blurRadius: 0
              )]
            ),
            child: lottie.Lottie.asset(
                // 'assets/lottie/circle.json',
                // 'https://assets8.lottiefiles.com/packages/lf20_LfVHSc.json',
                // 'https://assets8.lottiefiles.com/packages/lf20_sGOHY4.json',
              'assets/lottie/pink_circle.json',
                width: 20,
                height: 20,
                repeat: true,
                // reverse: true,
                fit: BoxFit.cover,
                alignment: Alignment.center
            ),
          ),
          Transform.rotate(
            angle: (e.heading * (math.pi / 180) * 1),
            // child: lottie.Lottie.asset('assets/lottie/arrow.json', width: 40, height: 40),
            child: Container(
              // decoration: BoxDecoration(
              //   shape: BoxShape.circle,
              //   boxShadow: [BoxShadow(
              //     color: Colors.pinkAccent.shade400,
              //     // spreadRadius: 0,
              //     // blurRadius: 1,
              //   )]
              // ),
              child: navigateIcon,
            ),
          ),
          // Text('        1'),
        ],
      );

      var marker = Marker(
        width: 80.0,
        height: 80.0,
        point: LatLng(e.lat, e.lon),
        builder: (ctx) => Container(
          child: Transform.translate(
            offset: Offset(0, 0),
            child: col,
          )
        ),
      );

      return MarkerLayerOptions(
        markers: [marker],
      );
    }).toList();

    var station = _stationPosition.map<MarkerLayerOptions>((e) {

      var metaInfo = Text(
        e.name ?? 'Unknown',
        style: TextStyle(color: Colors.pinkAccent, fontSize: 10),
      );

      var navigateIcon = Icon(
        Icons.pin_drop,
        color: Colors.indigo,
        size: 14,
      );

      var col = Stack(
        alignment: Alignment.center,
        children: [
          // metaInfo,
          // arrowStack,
          Container(
            // decoration: BoxDecoration(
            //   shape: BoxShape.circle,
            //   boxShadow: [BoxShadow(
            //     color: Colors.pinkAccent.shade400,
            //     // spreadRadius: 0,
            //     // blurRadius: 1,
            //   )]
            // ),
            child: navigateIcon,
          ),
          // Text('        1'),
        ],
      );

      var marker = Marker(
        width: 80.0,
        height: 80.0,
        point: LatLng(e.lat, e.lon),
        builder: (ctx) => Container(
            child: Transform.translate(
              offset: Offset(2, -24),
              child: col,
            )
        ),
      );

      return MarkerLayerOptions(
        markers: [marker],
      );
    }).toList();



    var info = Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [

        SizedBox(
          height: 40,
          width: MediaQuery.of(context).size.width,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: _customPosition?.length ?? 0,
              itemBuilder: (context, i) {

                var temp = _stationData[_customPosition[i].latestStation];

                var stationName = '?';
                if(temp != null) {
                  if(temp['name'] != null) {
                    stationName = temp['name'];
                  }
                }

                print(_stationData);

                // print(_customPosition[i].latestStation);

                return Padding(
                  padding: EdgeInsets.symmetric(horizontal: 3),
                  child: InputChip(
                    onPressed: () {},
                    label: Text('Car ' + _customPosition[i].name + ': ' + stationName.toString()),
                  ),
                );
              }
          ),
        ),
        // Container(
        //   color: Colors.white.withAlpha(150),
        //   height: 20,
        //   width: MediaQuery.of(context).size.width,
        //   child: Marquee(
        //     text: 'Some sample text that takes some space.',
        //     style: TextStyle(fontWeight: FontWeight.bold),
        //     scrollAxis: Axis.horizontal,
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     blankSpace: 20.0,
        //     velocity: 100.0,
        //     pauseAfterRound: Duration(seconds: 1),
        //     startPadding: 10.0,
        //     accelerationDuration: Duration(seconds: 1),
        //     accelerationCurve: Curves.linear,
        //     decelerationDuration: Duration(milliseconds: 500),
        //     decelerationCurve: Curves.easeOut,
        //   ),
        // )
      ],
    );

    var mapBody = FlutterMap(
      mapController: _mapController,
      options: MapOptions(
        // interactive: false,
        center: _center,
        minZoom: _zoom,
        maxZoom: _zoom,
      ),
      layers: <LayerOptions>[
        TileLayerOptions(
            urlTemplate:
            "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c']),
      ]
        + loc
        + station,
    );

    return Scaffold(
      // key: scaffoldKey,
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.transparent,

      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          title: Row(
            children: [
              lottie.Lottie.network(
                'https://assets10.lottiefiles.com/packages/lf20_dT1E1P.json',
                height: 60, repeat: true,
              ),
              SvgPicture.asset(
                'assets/logo2.svg',
                height: 40,
                color: Colors.indigo,
                clipBehavior: Clip.antiAlias,
              ),
            ],
          ),
          backgroundColor: Colors.white.withAlpha(00),
          elevation: 0.0,
          actions: [
            // Container(
            //   child: clockNow(),
            // ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                clockNow(),
              ],
            ),
            // Container(
            //   // color: Colors.white,
            //  decoration: BoxDecoration(
            //    shape: BoxShape.circle,
            //    color: Colors.white
            //  ),
            //  child: IconButton(
            //    iconSize: 15,
            //    onPressed: () {
            //      _mapController.move(LatLng(_defaultLat, _defaultLon), _zoom);
            //    },
            //    icon: Icon(Icons.cached, color: Color(0xD92E75FF)),
            //  ),
            // ),
            SizedBox(width: 10,)
          ],
        ),
      ),
      body: Stack(
        alignment: Alignment.centerRight,
        children: [
          mapBody,
          info
        ],
      ),
      // floatingActionButton: Stack(
      //   children: <Widget>[
      //     // Positioned(
      //     //   bottom: 10,
      //     //   right: 10,
      //     //   child: FloatingActionButton.extended(
      //     //     onPressed: () {
      //     //       _toggleListening();
      //     //       print("isPaused: ${isStreamStatePause()}");
      //     //     },
      //     //     label: _determineButtonIcon(),
      //     //     backgroundColor: _determineButtonColor(),
      //     //   ),
      //     // ),
      //     FloatingActionButton.extended(
      //       onPressed: () {
      //         _mapController.move(LatLng(_defaultLat, _defaultLon), _zoom);
      //       },
      //       label: Icon(Icons.cached, color: Color(0xD92E75FF)),
      //       backgroundColor: _determineButtonColor(),
      //     ),
      //   ],
      // ),
    );
  }

  @override
  void initState() {

    super.initState();

    if (_positionStreamSubscription == null) {
      final positionStream = fetchPosition();
      _positionStreamSubscription = positionStream.handleError((error) {
        print('STREAM_ERROR: $error');
        _positionStreamSubscription.cancel();
        _positionStreamSubscription = null;
      }).listen((position) => setState(() {
            // print('ListenPosition: $position');
            _customPosition = position;
          }));
      _positionStreamSubscription.pause();
    }

    setState(() {
      if (isStreamStatePause()) {
        _positionStreamSubscription.resume();
      }
    });

    fetchStation();

  }

  Color _determineButtonColor() {
    return Colors.white;
  }

  bool isStreamStatePause() {
    if (_positionStreamSubscription == null) return true;
    return _positionStreamSubscription.isPaused;
  }

  Icon _determineButtonIcon() {
    return isStreamStatePause()
        ? Icon(Icons.toggle_off, color: Color(0xD9FF2E2E))
        : Icon(Icons.toggle_on, color: Color(0xD92E75FF));
  }

  void _toggleListening() {
    setState(() {
      if (isStreamStatePause()) {
        _positionStreamSubscription?.resume();
      } else {
        _positionStreamSubscription?.pause();
      }
    });
  }

  @override
  void dispose() {
    if (_positionStreamSubscription != null) {
      _positionStreamSubscription.cancel();
      _positionStreamSubscription = null;
    }
    super.dispose();
  }
}
