class CarPosition {
  final String name;
  final double lat;
  final double lon;
  final double heading;
  final String desc;
  final String token;
  final String latestStation;

  CarPosition({this.name, this.lat=13.0, this.lon=100.0, this.heading = 0, this.desc, this.token, this.latestStation = ''});

  // CarPosition({this.lat=13.0, this.lon=100.0, this.heading = 0, this.label="Unknown"});

  @override
  String toString() {
    return 'Lat:$lat Lon:$lon Label:$name';
  }

}